# MyList 💻
Welcome to MyList! MyList is a dynamic web application where you must have an acconut to use it. There you can create your own lists with single click! Just enter your list name in the box 'List Name' and click on the botton 'Create'. MyList will show your newly created lists on the homepage as soon as you make them! MyList will also generate a custom URL with the title of your list. This website is made with Node.js, EJS as a template engine and Passport authentication middleware. ✨

# How to use this? 🤔
If your want to try this app on your local machine, follow these steps: 
1. Clone this repository in your local environment by the following command:
```git clone https://gitlab.com/natalijastojanovska/mylist.git```

2. Use NPM (Node Package Manager) to install dependencies for this project. <br>
```npm install```

3. Use MongoDB service to set up your database, than use Node.js to start the application: <br>
```node app.js```

4. Go to `localhost:3000` on your browser, and the homepage of MyLIst will appear. You can Register or Log in from here.

5. Now you are on MyLists tab where you can create new lists, and see already created, and if you click on a button Open it will link to the dynamically created page and URL for that list.

6. After that, you can add new items to the list or delete the unnecessary.


# How it works? 🛠
This web app uses Node.js for backend and Express.js for serving static files, using middlewares and generating URL's for lists with routes. The main part of the app is based on EJS, which is a template engine. As soon as you create the list, it gets saved on the MongoDB database with Mongoose. A new URL is also generated, as `/your-list-name` by which you can access that list. All the lists and items are saved on a MongoDB database. All these CRUD operations are carried out on MongoDB with Mongoose. The authentication is handled using Passport.js

# Inspiration 💡
This project was initially started as a course-along project which I made while learning Node.js, Express.js and Authentication.
